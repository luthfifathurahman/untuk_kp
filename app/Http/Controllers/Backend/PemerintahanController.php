<?php

namespace App\Http\Controllers\Backend;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Model\Pemerintahan;
use App\Model\Agama;
use App\Model\Pendidikan;
use App\Model\Jabatan;
use App\Model\JenisKelamin;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;
use Datatables;

class PemerintahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return view ('backend.pemerintahan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $agama = Agama::pluck('agama', 'id');
        $pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $jenkel = JenisKelamin::pluck('jenkel', 'id');
        $jabatan = Jabatan::pluck('jabatan', 'id');
        return view ('backend.pemerintahan.update',                 
        ['agama' => $agama, 
        'jenkel' => $jenkel, 
        'pendidikan' => $pendidikan,
        'jabatan' => $jabatan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new Pemerintahan();
        $data->nik = $request->nik;
        $data->nip = $request->nip;
		$data->nama = $request->nama;
        $data->avatar_id = $request->avatar_id;
        $data->tempat = $request->tempat;
        $data->tanggal = $request->tanggal;
        $data->jenkel_id = $request->jenkel_id;
        $data->jabatan_id = $request->jabatan_id;
        $data->pendidikan_id = $request->pendidikan_id;
        $data->agama_id = $request->agama_id;
		$data->active = $request->active;
		$data->user_modified = Session::get('userinfo')['user_id'];
		if($data->save()){
			return Redirect::to('/backend/pemerintahan')->with('success', "Data saved successfully")->with('mode', 'success');
		}

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $agama = Agama::pluck('agama', 'id');
        $pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $jenkel = JenisKelamin::pluck('jenkel', 'id');
        $jabatan = Jabatan::pluck('jabatan', 'id');
		$data = Pemerintahan::with(['user_modify'])->where('id', $id)->get();
		if ($data->count() > 0){
            return view ('backend.pemerintahan.view', 
            ['agama' => $agama, 
            'jenkel' => $jenkel, 
            'pendidikan' => $pendidikan,
            'jabatan' => $jabatan, 
            'data' => $data]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserLevel  $userLevel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agama = Agama::pluck('agama', 'id');
        $pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $jenkel = JenisKelamin::pluck('jenkel', 'id');
        $jabatan = Jabatan::pluck('jabatan', 'id');
		$data = Pemerintahan::where('id', $id)->where('active', '!=', 0)->get();
		if ($data->count() > 0){
            return view ('backend.pemerintahan.update', 
            ['agama' => $agama,
            'jenkel' => $jenkel, 
            'pendidikan' => $pendidikan,
            'jabatan' => $jabatan, 'data' => $data]);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Pemerintahan::find($id);
        $data->nik = $request->nik;
        $data->nip = $request->nip;
		$data->nama = $request->nama;
        $data->avatar_id = $request->avatar_id;
        $data->tempat = $request->tempat;
        $data->tanggal = $request->tanggal;
        $data->jenkel_id = $request->jenkel_id;
        $data->jabatan_id = $request->jabatan_id;
        $data->pendidikan_id = $request->pendidikan_id;
        $data->agama_id = $request->agama_id;
		$data->active = $request->active;
		$data->user_modified = Session::get('userinfo')['user_id'];
		if($data->save()){
			return Redirect::to('/backend/pemerintahan')->with('success', "Data saved successfully")->with('mode', 'success');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserLevel  $userLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
		$data = Pemerintahan::find($id);
		$data->active = 0;
		$data->user_modified = Session::get('userinfo')['user_id'];
		if($data->save()){
			Session::flash('success', 'Data deleted successfully');
			Session::flash('mode', 'success');
			return new JsonResponse(["status"=>true]);
		}else{
			return new JsonResponse(["status"=>false]);
		}
    }
	
	public function datatable() {	
		$userinfo = Session::get('userinfo');
		$data = Pemerintahan::where('active', '!=', 0);
	
        return Datatables::of($data)
			->addColumn('action', function ($data) {
				$userinfo = Session::get('userinfo');
				$access_control = Session::get('access_control');
				$segment =  \Request::segment(2);
				$url_edit = url('backend/pemerintahan/'.$data->id.'/edit');
                $url = url('backend/pemerintahan/'.$data->id);
				$view = "<a class='btn-action btn btn-primary btn-view' href='".$url."' title='View'><i class='fa fa-eye'></i></a>";
				$edit = "<a class='btn-action btn btn-info btn-edit' href='".$url_edit."' title='Edit'><i class='fa fa-edit'></i></a>";
                $delete = "<button data-url='".$url."' onclick='deleteData(this)' class='btn-action btn btn-danger btn-delete' title='Delete'><i class='fa fa-trash-o'></i></button>";
				if (!empty($access_control)) {
					if ($access_control[$userinfo['user_level_id']][$segment] == "v"){
						return $view;
					} else if ($access_control[$userinfo['user_level_id']][$segment] == "vu"){
						return $view." ".$edit;
					} else if ($access_control[$userinfo['user_level_id']][$segment] == "a"){
						return $view." ".$edit." ".$delete;
					}
				} else {
					return "";
				}
            })
            ->make(true);		
	}

	public function datatable_pemerintahan() {
		$data = Pemerintahan::select('pemerintahan.*')
		 ->where('pemerintahan.active', '!=', 0);
	
        return Datatables::of($data)
            ->make(true);		
    }

}