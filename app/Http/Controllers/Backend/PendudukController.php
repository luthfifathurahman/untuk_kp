<?php

namespace App\Http\Controllers\Backend;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Model\Supplier;
use App\Model\Agama;
use App\Model\Kewarganegaraan;
use App\Model\JenisKelamin;
use App\Model\Pekerjaan;
use App\Model\Pendidikan;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;
use Datatables;

class PendudukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return view ('backend.penduduk.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $agama = Agama::pluck('agama', 'id');
        $kewarganegaraan = Kewarganegaraan::pluck('kewarganegaraan', 'id');
        $pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $jenkel = JenisKelamin::pluck('jenkel', 'id');
        $pekerjaan = Pekerjaan::pluck('pekerjaan', 'id');
        return view ('backend.penduduk.update',
            ['agama' => $agama, 
            'kewarganegaraan' => $kewarganegaraan,
            'jenkel' => $jenkel,
            'pendidikan' => $pendidikan,
            'pekerjaan' => $pekerjaan,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //
        $data = new Supplier();
        $data->nik = $request->nik;
		$data->nama = $request->nama;
        $data->tempat = $request->tempat;
        $data->tgl_lahir = date('Y-m-d',strtotime($request->tgl_lahir));
        $data->jenkel_id = $request->jenkel_id;
        $data->alamat = $request->alamat;
        $data->rt = $request->rt;
        $data->kelurahan = $request->kelurahan;
        $data->kecamatan = $request->kecamatan;
        $data->kewarganegaraan_id = $request->kewarganegaraan_id;
        $data->agama_id = $request->agama_id;
        $data->pendidikan_id = $request->pendidikan_id;
        $data->pekerjaan_id = $request->pekerjaan_id;
        $data->active = $request->active;
		$data->user_modified = Session::get('userinfo')['user_id'];
		if($data->save()){
			return Redirect::to('/backend/penduduk')->with('success', "Data saved successfully")->with('mode', 'success');
		}

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $agama = Agama::pluck('agama', 'id');
        $kewarganegaraan = Kewarganegaraan::pluck('kewarganegaraan', 'id');
        $jenkel = JenisKelamin::pluck('jenkel', 'id');
        $pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $pekerjaan = Pekerjaan::pluck('pekerjaan', 'id');
		$data = Supplier::with(['user_modify'])->where('id', $id)->get();
		if ($data->count() > 0){
            return view ('backend.penduduk.view', 
                ['agama' => $agama, 
                'kewarganegaraan' => $kewarganegaraan,
                'jenkel' => $jenkel, 
                'pendidikan' => $pendidikan,
                'pekerjaan' => $pekerjaan,
                'data' => $data ]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserLevel  $userLevel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agama = Agama::pluck('agama', 'id');
        $kewarganegaraan = Kewarganegaraan::pluck('kewarganegaraan', 'id');
        $jenkel = JenisKelamin::pluck('jenkel', 'id');
        $pendidikan = Pendidikan::pluck('pendidikan', 'id');
        $pekerjaan = Pekerjaan::pluck('pekerjaan', 'id');
        $data = Supplier::where('id', $id)->where('active', '!=', 0)->get();
		if ($data->count() > 0){
            return view ('backend.penduduk.update', 
                ['agama' => $agama, 
                'kewarganegaraan' => $kewarganegaraan,
                'jenkel' => $jenkel,
                'pendidikan' => $pendidikan,
                'pekerjaan' => $pekerjaan,
                'data' => $data ]);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$data = Supplier::find($id);
        $data->nik = $request->nik;
		$data->nama = $request->nama;
        $data->tempat = $request->tempat;
        $data->tgl_lahir = date('Y-m-d',strtotime($request->tgl_lahir));
        $data->jenkel_id = $request->jenkel_id;
        $data->rt = $request->rt;
        $data->alamat = $request->alamat;
        $data->kelurahan = $request->kelurahan;
        $data->kecamatan = $request->kecamatan;
        $data->kewarganegaraan_id = $request->kewarganegaraan_id;
        $data->agama_id = $request->agama_id;
        $data->pendidikan_id = $request->pendidikan_id;
        $data->pekerjaan_id = $request->pekerjaan_id;
        $data->active = $request->active;
		$data->user_modified = Session::get('userinfo')['user_id'];
		if($data->save()){
			return Redirect::to('/backend/penduduk')->with('success', "Data saved successfully")->with('mode', 'success');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserLevel  $userLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
		$data = Supplier::find($id);
		$data->active = 0;
		$data->user_modified = Session::get('userinfo')['user_id'];
		if($data->save()){
			Session::flash('success', 'Data deleted successfully');
			Session::flash('mode', 'success');
			return new JsonResponse(["status"=>true]);
		}else{
			return new JsonResponse(["status"=>false]);
		}
    }
	
	public function datatable() {	
		$userinfo = Session::get('userinfo');
		$data = Supplier::where('active', '!=', 0);
	
        return Datatables::of($data)
			->addColumn('action', function ($data) {
				$userinfo = Session::get('userinfo');
				$access_control = Session::get('access_control');
				$segment =  \Request::segment(2);
				$url_edit = url('backend/penduduk/'.$data->id.'/edit');
				$url = url('backend/penduduk/'.$data->id);
				$view = "<a class='btn-action btn btn-primary btn-view' href='".$url."' title='View'><i class='fa fa-eye'></i></a>";
				$edit = "<a class='btn-action btn btn-info btn-edit' href='".$url_edit."' title='Edit'><i class='fa fa-edit'></i></a>";
				$delete = "<button data-url='".$url."' onclick='deleteData(this)' class='btn-action btn btn-danger btn-delete' title='Delete'><i class='fa fa-trash-o'></i></button>";
				if (!empty($access_control)) {
					if ($access_control[$userinfo['user_level_id']][$segment] == "v"){
						return $view;
					} else if ($access_control[$userinfo['user_level_id']][$segment] == "vu"){
						return $view." ".$edit;
					} else if ($access_control[$userinfo['user_level_id']][$segment] == "a"){
						return $view." ".$edit." ".$delete;
					}
				} else {
					return "";
				}
            })
            ->editColumn('alamat', function($data) {
                return str_ireplace("\r\n", ', ', $data->alamat);
            })		
            ->make(true);		
	}

	public function datatable_penduduk() {
		$data = Supplier::select('supplier.*')
		 ->where('supplier.active', '!=', 0);
	
        return Datatables::of($data)
            ->editColumn('alamat', function($data) {
                return str_ireplace("\r\n", ', ', $data->alamat);
            })
            ->make(true);		
	}    
}
