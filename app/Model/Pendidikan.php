<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    //
    protected $table = 'ref_pendidikan';
	protected $hidden = ['created_at', 'updated_at'];
}
