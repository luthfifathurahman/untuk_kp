<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisKelamin extends Model
{
    //
    protected $table = 'ref_jenkel';
	protected $hidden = ['created_at', 'updated_at'];
}
