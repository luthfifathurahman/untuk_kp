<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pemerintahan extends Model
{
    //
    protected $table = 'pemerintahan';
	protected $hidden = ['created_at', 'updated_at'];

	public function avatar()
	{
		return $this->belongsTo('App\Model\MediaLibrary', 'avatar_id');
	}
	
	
	public function user_modify()
	{
		return $this->belongsTo('App\Model\User', 'user_modified');
    }
    
    public function agama()
	{
		return $this->belongsTo('App\Model\Agama', 'agama_id');
    }
    
	public function media_image_1()
	{
		return $this->belongsTo('App\Model\MediaLibrary', 'avatar_id');
    }
    
	public function jenkel()
	{
		return $this->belongsTo('App\Model\JenisKelamin', 'jenkel_id');
    }
    
    public function jabatan()
	{
		return $this->belongsTo('App\Model\Jabatan', 'jabatan_id');
	}
	
	public function pendidikan()
	{
		return $this->belongsTo('App\Model\Pendidikan', 'pendidikan_id');
	}
}
