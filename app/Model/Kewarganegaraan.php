<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kewarganegaraan extends Model
{
    //
    protected $table = 'ref_kewarganegaraan';
	protected $hidden = ['created_at', 'updated_at'];
}
