<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefPemerintahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_pemerintahan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik',16);
            $table->string('nip',16);
            $table->string('nama',100);
            $table->integer('avatar_id')->length(11)->unsigned();;
            $table->string('tempat',50);
            $table->date('tanggal');
            $table->integer('jenkel_id')->length(11)->unsigned();;
            $table->integer('pendidikan_id')->length(11)->unsigned();;
            $table->integer('agama_id')->length(11)->unsigned();;
            $table->integer('jabatan_id')->length(11)->unsigned();;
            $table->integer('user_modified')->length(11)->unsigned();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_pemerintahan');
    }
}
