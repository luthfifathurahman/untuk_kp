<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendudukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penduduk', function (Blueprint $table) {
            $table->increments('id');
            $table->char('nik',16);
            $table->char('nama',100);
            $table->char('tempat',30);
            $table->date('tgl_lahir');
            $table->char('jenkel',20);
            $table->char('goldar',20);
            $table->char('rt',10);
            $table->char('rw',10);
            $table->char('kelurahan',30);
            $table->char('kecamatan',30);
            $table->char('agama',20);
            $table->char('status',20);
            $table->char('pendidikan',30);
            $table->char('pekerjaan',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penduduk');
    }
}
