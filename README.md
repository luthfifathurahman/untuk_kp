## Dokumentasi


Software yang harus di download :
    1. Composer
    2. Git Bash

--------------------------------------------------------------------------------------------------------------------------


Cara clone repo

1 - Clone repo

    Clone repo ini dengan cara ketik yang di bawah ini pada git bash. 
    Pastikan untuk menempatkan file tersebut ke xampp/htdocs. 
    Untuk tutorial gitlab, tersedia di youtube. 
    Jika sebelumnya sudah memiliki folder untuk_kp di htdocs, 
    bisa diganti terlebih dahulu agar proses clone tidak gagal

        git clone https://gitlab.com/luthfifathurahman/untuk_kp.git


2- Akses ke repo

    Setelah clone, langsung bisa diakses dengan cara:

        cd untuk_kp


3- Install the project dependencies dari composer

    Ini untuk menginstall file-file tidak masuk saat diclone awal. 
    Yang pasti, harus menginstall composer dulu baru menjalankan program di bawah ini. 
    Link install composer adalah https://getcomposer.org/download/ 
    Setelah selesai install composer, baru jalankan perintah di bawah ini :
    
    composer install

--------------------------------------------------------------------------------------------------------------------------

Cara menjalankan filenya

1- Membuat .env file

    Buat file .env (pada folder untuk_kp). isi dari file .env diambil dari file .env.example


2- Menggunakan file .env

    File tersebut berisi data tentang database. 
    Isi bagian DB_DATABASE dengan nama database yang ingin kalian gunakan
    Hal ini berhubungan dengan phpmyadmin, jadi aktifkan dulu xamppnya.

    Note: Isi bagian DB_DATABASE saja. Jangan dikotak-katik yang lain


3- Generate encryption key

    Gatau buat apa, cuma harus dijalankan. 
    Ketika perintah sudah dijalankan, maka bagian APP_KEY pada .env akan terisi.
    Silahkan dijalankan perintahnya:

    php artisan key:generate


4- Menjalankan Aplikasi

    Untuk menjalankannya, ketik perintah di bawah ini pada git bash

    php artisan serve

--------------------------------------------------------------------------------------------------------------------------


Jika ingin membantu...

1- Ketik perintah ini pada git bash

    git branch namayangdiinginkan


2- Lalu ketik:

    git checkout namayangdiinginkan


3- Kemudian ketik:

    git add .


4- Kemudian ketik:

    git commit -m komentaryangdibuat

    (Komentar wajib diisi, biar tau apa yang berubah. isi saja nama file/fitur yang diubah)


5- Terakhir ketik:

    git push origin namayangdiinginkan



## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
