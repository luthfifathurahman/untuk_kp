<?php
	$breadcrumb = [];
	$breadcrumb[0]['title'] = 'Dashboard';
	$breadcrumb[0]['url'] = url('backend/dashboard');
	$breadcrumb[1]['title'] = 'Data Keluarga';
	$breadcrumb[1]['url'] = url('backend/identitas');
	$breadcrumb[2]['title'] = 'Tambah';
	$breadcrumb[2]['url'] = url('backend/identitas/update');
?>


<!-- LAYOUT -->
@extends('backend.layouts.main')

<!-- TITLE -->
@section('title', 'Update Identitas Kelurahan')

@endsection

<!-- CONTENT -->
@section('content')
	<?php
		$nip = old('nip');
		$nik = old('nik');
		$nama = old('nama');
		$avatar_id = old('avatar_id', 0);
        $tempat = old('tempat');
        $tanggal = date('Y-m-d');
        $jenkel_id = old('jenkel_id');
        $agama_id = old('agama_id');
        $pendidikan_id = old('pendidikan_id');
        $jabatan_id = old('jabatan_id');
		$active = 1;
		$method = "POST";
		$mode = "Tambah";
		$url = "backend/pemerintahan/";
    ?>
    
	<div class="page-title">
		<div class="title_left">
			<h3><?=$mode;?></h3>
		</div>
		<div class="title_right">
			<div class="col-md-4 col-sm-4 col-xs-8 form-group pull-right top_search">
				<a href="<?=url('/backend/pemerintahan');?>" class="btn-index btn btn-primary btn-block" title="Back"><i class="fa fa-arrow-left"></i></a>
			</div>
        </div>
        <div class="clearfix"></div>
		@include('backend.elements.breadcrumb',array('breadcrumb' => $breadcrumb))
	</div>
	<div class="clearfix"></div>
	<br/><br/>	
	<div class="row">
		<div class="col-xs-12">
			<div class="x_panel">
				<div class="x_content">
					{{ Form::open(['url' => $url, 'method' => $method,'class' => 'form-horizontal form-label-left']) }}
                        {!! csrf_field() !!}
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">NIP <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="nip" name="nip" required="required" class="form-control col-md-7 col-xs-12" value="<?=$nip;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">NIK <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="nik" name="nik" required="required" class="form-control col-md-7 col-xs-12" value="<?=$nik;?>" autofocus>
							</div>
                        </div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Nama <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" value="<?=$nama;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Foto </label>
							<div class="col-sm-6 col-xs-9">
								<input type="hidden" name="avatar_id" value=<?=$avatar_id;?> id="id-cover-image_1">
								@include('backend.elements.change_cover',array('cover' => $cover_1, 'id_count' => 1))	
							</div>
						</div>
						<div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Tempat / Tanggal Lahir <span class="required">*</span></label>
							<div class="col-sm-4 col-xs-6">
								<input type="text" id="tempat" name="tempat" required="required" class="form-control col-md-7 col-xs-12" value="<?=$tempat;?>" autofocus>
							</div>
							<div class="col-sm-3 col-xs-5">
                                <div class='input-group date' id='myDatepicker'>
                                    <input type='text' class="form-control" / name="tanggal" value="<?=$tanggal;?>">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Jenis Kelamin <span class="required">*</span></label>
							<div class="col-sm-3 col-xs-5">
								{{
									Form::select(
										'jenkel_id', 
										$jenkel,
										$jenkel_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Jenis Kelamin'
										))
										
								}}					
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Agama <span class="required">*</span></label>
							<div class="col-sm-3 col-xs-5">
								{{
									Form::select(
										'agama_id', 
										$agama,
										$agama_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Agama'
										))
										
								}}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Pendidikan <span class="required">*</span></label>
							<div class="col-sm-4 col-xs-12">
								{{
									Form::select(
										'pendidikan_id', 
										$pendidikan,
										$pendidikan_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Pendidikan'
										))
										
								}}		
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Jabatan <span class="required">*</span></label>
							<div class="col-sm-4 col-xs-12">
								{{
									Form::select(
										'jabatan_id', 
										$jabatan,
										$jabatan_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Jabatan'
										))
										
								}}		
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Status: </label>
							<div class="col-sm-5 col-xs-12">
								{{
								Form::select(
									'active',
									['1' => 'Active', '2' => 'Deactive'],
									$active,
									array(
										'class' => 'form-control',
									))
								}}								
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-sm-6 col-xs-12 col-sm-offset-3">
								<a href="<?=url('/backend/pemerintahan')?>" class="btn btn-warning">Cancel</a>
								<button type="submit" class="btn btn-primary">Submit </button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection

<!-- CSS -->
@section('css')

@endsection

<!-- JAVASCRIPT -->
@section('script')
	<script>
        $('#myDatepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
	</script>
	
	@include('backend.partials.colorbox')	
@endsection