@extends('backend.layouts.login')

@section('title', 'Login')

@section('content')
    <div>
        <div class="login-clean ">
            <form method="post" id="formLogin">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <div>
                    <h1><center><a href="<?=url('/');?>"><img src="<?=url(getData('logo'));?>"; class='img-responsive' style="max-width:100px;"></a></center></h1>   
                </div>
                <br>
                <div>
                    <h1><center><label class="control-label" ><?=getData('web_description');?></label><center><h1>
                </div>
                <br>
                <div class="error-alert"></div>
                <div class="form-group"><input class="form-control" type="email" name="email" required="" placeholder="Email"></div>
                <br>
                <div class="form-group"><input class="form-control" type="password" name="password" required="" placeholder="Password" ></div>
                
                <div class="form-group"><button type="submit" class="btn btn-submit btn-primary ladda-button btn-block">Login</button></div>
                <div class="clearfix"></div>
                <div class="separator">
                <div class="clearfix"></div>
                <div>
                    <center><p>&copy;2021 <b>Version</b> 0.3.9-beta <strong>All rights reserved.</strong></p></center>
                </div>
                </div>
            </form>
        </div>
    </div>
@endsection
