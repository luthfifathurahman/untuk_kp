<?php
	if (!empty($data)):
		$data = $data[0];
?>
	<div class="x_panel">
		<div class="x_content">
			<div class="form-group col-xs-12">
				<label class="control-label">NIK :</label>
				<div class="detail-view"><?=nl2br($data->nik);?></div>
            </div>
			<div class="form-group col-xs-12">
				<label class="control-label">Nama :</label>
				<div class="detail-view"><?=nl2br($data->nama);?></div>
            </div>
			<div class="form-group col-xs-12">
				<label class="control-label">Tempat Lahir :</label>
				<div class="detail-view"><?=nl2br($data->tempat);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Tanggal Lahir :</label>
				<span class="form-control"><?=date('d M Y', strtotime($data->tgl_lahir));?></span>
			</div>
			
			<div class="form-group col-xs-12">
				<label class="control-label">Lingkungan :</label>
				<div class="detail-view"><?=nl2br($data->rt);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Kelurahan :</label>
				<div class="detail-view"><?=nl2br($data->kelurahan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Kecamatan :</label>
				<div class="detail-view"><?=nl2br($data->kecamatan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Kewarganegaraan :</label>
				<div class="detail-view"><?=nl2br($data->kewarganegaraan->kewarganegaraan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Agama :</label>
				<div class="detail-view"><?=nl2br($data->agama->agama);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Pendidikan :</label>
				<div class="detail-view"><?=nl2br($data->pendidikan->pendidikan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Pekerjaan :</label>
				<div class="detail-view"><?=nl2br($data->pekerjaan->pekerjaan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Status :</label>
				<span class="form-control">
					<?php
						if ($data->active == 1){
							$text = "Active";
							$label = "success";
						} else 
						if ($data->active == 2){
							$text = "Deactive";
							$label = "warning";
						}
						echo "<span class='badge badge-" . $label . "'>". $text . "</span>";
					
					?>
				</span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Date Created :</label>
				<span class="form-control"><?=date('d M Y H:i:s', strtotime($data->created_at));?></span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Last Modified :</label>
				<span class="form-control"><?=date('d M Y H:i:s', strtotime($data->updated_at));?></span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Last Modified by :</label>
				<span class="form-control"><?=$data->user_modify->username;?></span>
			</div>
		</div>
	</div>
<?php
	endif;
?>

