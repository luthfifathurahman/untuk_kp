<?php
	$breadcrumb = [];
	$breadcrumb[0]['title'] = 'Dashboard';
	$breadcrumb[0]['url'] = url('backend/dashboard');
	$breadcrumb[1]['title'] = 'Data Penduduk';
	$breadcrumb[1]['url'] = url('backend/penduduk');	
	$breadcrumb[2]['title'] = 'Tambah Data Penduduk';
	$breadcrumb[2]['url'] = url('backend/penduduk/create');
	if (isset($data)){
		$breadcrumb[2]['title'] = 'Edit';
		$breadcrumb[2]['url'] = url('backend/penduduk/'.$data[0]->id.'/edit');
	}
?>

<!-- LAYOUT -->
@extends('backend.layouts.main')

<!-- TITLE -->
@section('title')
	<?php
		$mode = "Tambah";
		if (isset($data)){
			$mode = "Edit";
		}
	?>
    <?=$mode;?> Data Penduduk
@endsection

<!-- CONTENT -->
@section('content')
	<?php
	    $nik = old('nik');
		$nama = old('nama');
        $tempat = old('tempat');
        $tgl_lahir = date('Y-m-d');
        $jenkel_id = old('jenkel_id');
        $alamat = old('alamat');
        $rt = old('rt');
        $kelurahan = old('kelurahan');
        $kecamatan = old('kecamatan');
        $kewarganegaraan_id = old('kewarganegaraan_id');
        $agama_id = old('agama_id');
        $pendidikan_id = old('pendidikan_id');
        $pekerjaan_id = old('pekerjaan_id');
		$active = 1;
		$method = "POST";
		$mode = "Tambah";
		$url = "backend/penduduk/";
		if (isset($data)){
			$nik = $data[0]->nik;
            $nama = $data[0]->nama;
            $tempat = $data[0]->tempat;
            $tgl_lahir = date('Y-m-d',strtotime($data[0]->tgl_lahir));
			$jenkel_id = $data[0]->jenkel_id;
			$alamat = $data[0]->alamat;
			$rt = $data[0]->rt;
			$kelurahan = $data[0]->kelurahan;
			$kecamatan = $data[0]->kecamatan;
			$kewarganegaraan_id = $data[0]->kewarganegaraan_id;
			$agama_id = $data[0]->agama_id;
			$pendidikan_id = $data[0]->pendidikan_id;
			$pekerjaan_id = $data[0]->pekerjaan_id;
			$active = $data[0]->active;
			$method = "PUT";
			$mode = "Edit";
			$url = "backend/penduduk/".$data[0]->id;
		}
	?>
	<div class="page-title">
		<div class="title_left">
			<h3><?=$mode;?> Data Penduduk </h3>
		</div>
		<div class="title_right">
			<div class="col-md-4 col-sm-4 col-xs-8 form-group pull-right top_search">
                @include('backend.elements.back_button',array('url' => '/backend/penduduk'))
			</div>
        </div>
        <div class="clearfix"></div>
		@include('backend.elements.breadcrumb',array('breadcrumb' => $breadcrumb))
	</div>
	<div class="clearfix"></div>
	<br/><br/>	
	<div class="row">
		<div class="col-xs-12">
			<div class="x_panel">
				<div class="x_content">
					{{ Form::open(['url' => $url, 'method' => $method,'class' => 'form-horizontal form-label-left']) }}
						{!! csrf_field() !!}
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">NIK <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="nik" name="nik" required="required" class="form-control col-md-7 col-xs-12" value="<?=$nik;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Nama <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" value="<?=$nama;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Tempat / Tanggal Lahir <span class="required">*</span></label>
							<div class="col-sm-4 col-xs-6">
								<input type="text" id="tempat" name="tempat" required="required" class="form-control col-md-7 col-xs-12" value="<?=$tempat;?>" autofocus>
							</div>
							<div class="col-sm-3 col-xs-5">
                                <div class='input-group date' id='myDatepicker'>
                                    <input type='text' class="form-control" / name="tgl_lahir" value="<?=$tgl_lahir;?>">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Jenis Kelamin <span class="required">*</span></label>
							<div class="col-sm-3 col-xs-5">
								{{
									Form::select(
										'jenkel_id', 
										$jenkel,
										$jenkel_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Jenis Kelamin'
										))
										
								}}					
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Alamat <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<textarea id="alamat" name="alamat" required="required" class="form-control col-md-7 col-xs-12" rows=5><?=$alamat;?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Lingkungan</label>
							<div class="col-sm-1 col-xs-2">
								<input type="text" id="rt" name="rt" class="form-control col-md-7 col-xs-12" value="<?=$rt;?>" autofocus>								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Kelurahan <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="kelurahan" name="kelurahan" required="required" class="form-control col-md-7 col-xs-12" value="<?=$kelurahan;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Kecamatan <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="kecamatan" name="kecamatan" required="required" class="form-control col-md-7 col-xs-12" value="<?=$kecamatan;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Kewarganegaraan <span class="required">*</span></label>
							<div class="col-sm-3 col-xs-5">
								{{
									Form::select(
										'kewarganegaraan_id',
										$kewarganegaraan,
										$kewarganegaraan_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Kewarganegaraan'
										))
									}}		
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Agama <span class="required">*</span></label>
							<div class="col-sm-3 col-xs-5">
								{{
									Form::select(
										'agama_id', 
										$agama,
										$agama_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Agama'
										))
										
								}}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Pendidikan <span class="required">*</span></label>
							<div class="col-sm-4 col-xs-12">
								{{
									Form::select(
										'pendidikan_id', 
										$pendidikan,
										$pendidikan_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Pendidikan'
										))
										
								}}		
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Pekerjaan <span class="required">*</span></label>
							<div class="col-sm-4 col-xs-12">
								{{
									Form::select(
										'pekerjaan_id', 
										$pekerjaan,
										$pekerjaan_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Pekerjaan'
										))
										
								}}		
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Status: </label>
							<div class="col-sm-3 col-xs-4">
								{{
								Form::select(
									'active',
									['1' => 'Active', '2' => 'Deactive'],
									$active,
									array(
										'class' => 'form-control',
									))
								}}								
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-sm-6 col-xs-12 col-sm-offset-3">
								<a href="<?=url('/backend/penduduk')?>" class="btn btn-warning">Cancel</a>
								<button type="submit" class="btn btn-primary">Submit </button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection

<!-- CSS -->
@section('css')

@endsection

<!-- JAVASCRIPT -->
@section('script')
	<script>
        $('#myDatepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
	</script>
	
	@include('backend.partials.colorbox')	
@endsection