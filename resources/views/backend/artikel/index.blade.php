<?php
	$breadcrumb = [];
	$breadcrumb[0]['title'] = 'Dashboard';
	$breadcrumb[0]['url'] = url('backend/dashboard');
	$breadcrumb[1]['title'] = 'Artikel';
	$breadcrumb[1]['url'] = url('backend/artikel');
?>

<!-- LAYOUT -->
@extends('backend.layouts.main')

<!-- TITLE -->
@section('title', 'Artikel')

<!-- CONTENT -->
@section('content')
	<div class="page-title">
		<div class="title_left">
			<h3>Artikel</h3>
		</div>
		<div class="title_right">
			<div class="col-md-4 col-sm-4 col-xs-8 form-group pull-right top_search">
				@include('backend.elements.create_button',array('url' => '/backend/artikel/tambah'))
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	@include('backend.elements.breadcrumb',array('breadcrumb' => $breadcrumb))	
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_content">
                    @include('backend.elements.notification')
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="berita-tab" data-toggle="tab" href="#berita" role="tab" aria-controls="berita" aria-selected="false">Berita Desa</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="halstatis-tab" data-toggle="tab" href="#halstatis" role="tab" aria-controls="halstatis" aria-selected="false">Halaman Statis</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade active in" id="berita" role="tabpanel" aria-labelledby="berita-tab">
                                <table class="table table-striped table-hover table-bordered dt-responsive nowrap dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th> 
                                            <th>Judul</th>
                                            <th>Isi</th>
                                            <th>Kategori</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="halstatis" role="tabpanel" aria-labelledby="halstatis-tab">
                                <table class="table table-striped table-hover table-bordered dt-responsive nowrap dataTable2" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th> 
                                            <th>Nama</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Nama</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>					
	</div>
@endsection

<!-- CSS -->
@section('css')

@endsection

<!-- JAVASCRIPT -->
@section('script')
	<script>
		$('.dataTable').dataTable({
			processing: true,
			serverSide: true,
			ajax: "<?=url('backend/artikel/datatable');?>",
			columns: [
				{ "data": null,"sortable": false, orderable: false, searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					}  
				},
				{data: 'judul', name: 'judul'},
				{data: 'isi', name: 'isi'},
				{data: 'kategori_id', name: 'kategori_id'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			],
			responsive: true
		});
    </script>
    
    <script>
		$('.dataTable2').dataTable({
			processing: true,
			serverSide: true,
			ajax: "<?=url('backend/keluarga/datatable');?>",
			columns: [
				{ "data": null, "sortable": false, orderable: false, searchable: false,
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                    
				},
				{data: 'no_kk', name: 'no_kk'},
                {data: 'nama', name: 'nama'},
                {data: 'alamat', name: 'alamat'},
				{data:  'lingkungan', name: 'lingkungan'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			],
			responsive: true
		});
	</script>
@endsection