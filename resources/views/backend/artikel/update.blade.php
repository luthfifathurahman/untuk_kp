<?php
	$breadcrumb = [];
	$breadcrumb[0]['title'] = 'Dashboard';
	$breadcrumb[0]['url'] = url('backend/dashboard');
	$breadcrumb[1]['title'] = 'Artikel';
	$breadcrumb[1]['url'] = url('backend/artikel');
	$breadcrumb[2]['title'] = 'Tambah Artikel';
    $breadcrumb[2]['url'] = url('backend/artikel/tambah');
    if (isset($data)){
		$breadcrumb[2]['title'] = 'Ubah Artikel';
		$breadcrumb[2]['url'] = url('backend/artikel/'.$data[0]->id.'/ubah');
	}
?>


<!-- LAYOUT -->
@extends('backend.layouts.main')

<!-- TITLE -->
@section('title')
<?php
		$mode = "Tambah";
		if (isset($data)){
			$mode = "Ubah";
		}
	?>
    <?=$mode;?> Artikel

@endsection

<!-- CONTENT -->
@section('content')
    <?php
        $judul = old('judul');
        $isi = old('isi');
        $kategori_id = old('kategori_id');
        $active = 1;
        $method = "POST";
        $mode = "Tambah";
        $url = "backend/artikel/";
        if (isset($data)){
            $judul = $data[0]->judul;
            $isi = $data[0]->isi;
            $kategori_id = $data[0]->kategori_id;
            $active = $data[0]->active;
            $method = "PUT";
            $mode = "Ubah";
            $url = "backend/artikel/".$data[0]->id;
        }
    ?>
    
	<div class="page-title">
		<div class="title_left">
			<h3><?=$mode;?> Artikel</h3>
		</div>
		<div class="title_right">
			<div class="col-md-4 col-sm-4 col-xs-8 form-group pull-right top_search">
				<a href="<?=url('/backend/artikel');?>" class="btn-index btn btn-primary btn-block" title="Back"><i class="fa fa-arrow-left"></i></a>
			</div>
        </div>
        <div class="clearfix"></div>
		@include('backend.elements.breadcrumb',array('breadcrumb' => $breadcrumb))
	</div>
	<div class="clearfix"></div>
	<br/><br/>	
	<div class="row">
		<div class="col-xs-12">
			<div class="x_panel">
				<div class="x_content">
					{{ Form::open(['url' => $url, 'method' => $method,'class' => 'form-horizontal form-label-left']) }}
                        {!! csrf_field() !!}
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Judul <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="judul" name="judul" required="required" class="form-control col-md-7 col-xs-12" value="<?=$judul;?>" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Isi Artikel <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<textarea type="text" id="isi" name="isi" required="required" class="form-control" value="<?=$isi;?>" autofocus></textarea>
							</div>
                        </div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Kategori <span class="required">*</span></label>
							<div class="col-sm-3 col-xs-5">
								{{
									Form::select(
										'kategori_id', 
										$kategori,
										$kategori_id,
										array(
											'class' => 'form-control',
											'placeholder' => 'Pilih Kategori'
										))
										
								}}					
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Status: </label>
							<div class="col-sm-5 col-xs-12">
								{{
								Form::select(
									'active',
									['1' => 'Active', '2' => 'Deactive'],
									$active,
									array(
										'class' => 'form-control',
									))
								}}								
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-sm-6 col-xs-12 col-sm-offset-3">
								<a href="<?=url('/backend/artikel')?>" class="btn btn-warning">Cancel</a>
								<button type="submit" class="btn btn-primary">Submit </button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection

<!-- CSS -->
@section('css')

@endsection

<!-- JAVASCRIPT -->
@section('script')
    <script>
        CKEDITOR.replace( 'isi' );
    </script>
	@include('backend.partials.colorbox')	
@endsection