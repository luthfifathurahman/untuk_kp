<?php
	$breadcrumb = [];
	$breadcrumb[0]['title'] = 'Dashboard';
	$breadcrumb[0]['url'] = url('backend/dashboard');
	$breadcrumb[1]['title'] = 'Data Keluarga';
	$breadcrumb[1]['url'] = url('backend/keluarga');
	$breadcrumb[2]['title'] = 'Tambah';
	$breadcrumb[2]['url'] = url('backend/keluarga/tambah');
	if (isset($data)){
		$breadcrumb[2]['title'] = 'Edit';
		$breadcrumb[2]['url'] = url('backend/keluarga/'.$data[0]->id.'/edit');
	}
?>

<!-- LAYOUT -->
@extends('backend.layouts.main')

<!-- TITLE -->
@section('title')
	<?php
		$mode = "Tambah";
		if (isset($data)){
			$mode = "Edit";
		}
	?>
    <?=$mode;?> - Data Keluarga
@endsection

<!-- CONTENT -->
@section('content')
    <?php
        $no_kk = old('no_kk');
        $nama = old('nama');
        $alamat = old('alamat');
        $lingkungan = old('lingkungan');
		$active = 1;
		$method = "POST";
		$mode = "Tambah";
		$url = "backend/keluarga/";
		if (isset($data)){
            $no_kk = $data[0]->no_kk;
            $nama = $data[0]->nama;
            $alamat = $data[0]->alamat;
            $lingkungan = $data[0]->lingkungan;
            $active = $data[0]->active;
			$method = "PUT";
			$mode = "Edit";
			$url = "backend/keluarga/".$data[0]->id;
		}
	?>
	<div class="page-title">
		<div class="title_left">
			<h3><?=$mode;?> - Data Keluarga </h3>
		</div>
		<div class="title_right">
			<div class="col-md-4 col-sm-4 col-xs-8 form-group pull-right top_search">
				<a href="<?=url('/backend/keluarga');?>" class="btn-index btn btn-primary btn-block" title="Back"><i class="fa fa-arrow-left"></i></a>
			</div>
        </div>
        <div class="clearfix"></div>
		@include('backend.elements.breadcrumb',array('breadcrumb' => $breadcrumb))
	</div>
	<div class="clearfix"></div>
	<br/><br/>	
	<div class="row">
		<div class="col-xs-12">
			<div class="x_panel">
				<div class="x_content">
					{{ Form::open(['url' => $url, 'method' => $method,'class' => 'form-horizontal form-label-left']) }}
                        {!! csrf_field() !!}
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">No KK <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="no_kk" name="no_kk" required="required" class="form-control col-md-7 col-xs-12" value="<?=$no_kk;?>" autofocus>
							</div>
                        </div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Nama Kepala Keluarga <span class="required">*</span></label>
							<div class="col-sm-7 col-xs-12">
								<input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" value="<?=$nama;?>" autofocus>
							</div>
                        </div>
    					<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Alamat <span class="required">*</span></label>
							<div class="col-sm-9 col-xs-12">
								<textarea id="alamat" name="alamat" required="required" class="form-control col-md-7 col-xs-12" rows=5><?=$alamat;?></textarea>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-sm-3 col-xs-12">Lingkungan <span class="required">*</span></label>
                            <div class="col-md-5 col-xs-12">
                                <input type="text" name="lingkungan" required="required" class="form-control col-md-7 col-xs-12" value="<?=$lingkungan;?>">
                            </div>
                        </div>
						<div class="form-group">
							<label class="control-label col-sm-3 col-xs-12">Status: </label>
							<div class="col-sm-5 col-xs-12">
								{{
								Form::select(
									'active',
									['1' => 'Active', '2' => 'Deactive'],
									$active,
									array(
										'class' => 'form-control',
									))
								}}								
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-sm-6 col-xs-12 col-sm-offset-3">
								<a href="<?=url('/backend/keluarga')?>" class="btn btn-warning">Cancel</a>
								<button type="submit" class="btn btn-primary">Submit </button>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@endsection

<!-- CSS -->
@section('css')

@endsection

<!-- JAVASCRIPT -->
@section('script')

@endsection