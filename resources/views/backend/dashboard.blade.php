<?php
	$breadcrumb = [];
	$breadcrumb[0]['title'] = 'Dashboard';
	$breadcrumb[0]['url'] = url('backend/dashboard');
?>

<!-- LAYOUT -->
@extends('backend.layouts.main')

<!-- TITLE -->
@section('title', 'Dashboard')

<!-- CONTENT -->
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Beranda</h3>
        </div>
        <div class="title_right">
        </div>
    </div>
    
    <div class="clearfix"></div>
    <div class="row">

        
        <div class="col-xs-12">
            <div class="row">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-caret-square-o-right"></i>
                    </div>
                    <div class="count">179</div>

                    <h3>Penduduk</h3>
                    <p>Jumlah Penduduk <?=getData('web_title');?></p>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i>
                    </div>
                    <div class="count">179</div>

                    <h3>Keluarga</h3>
                    <p>Jumlah Keluarga <?=getData('web_title');?></p>
                  </div>
                </div>
            </div>
        </div>
    </div>


@endsection