<?php
	if (!empty($data)):
        $data = $data[0];
?>
	<div class="x_panel">
		<div class="x_content">
			<div class="form-group col-xs-12">
				<label class="control-label">NIK :</label>
				<span class="form-control"><?=$data->nik;?></span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">NIP :</label>
				<span class="form-control"><?=$data->nip;?></span>
            </div>
			<div class="form-group col-xs-12">
				<label class="control-label">Nama :</label>
				<span class="form-control"><?=$data->nama;?></span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Avatar :</label><br/>
				<img width=300 class="img-responsive" src="<?=url('upload/img/thumbnails/'.$data->avatar->name.".".$data->avatar->type);?>"><br/>
            </div>
			<div class="form-group col-xs-12">
				<label class="control-label">Tempat Lahir :</label>
				<div class="detail-view"><?=nl2br($data->tempat);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Tanggal Lahir :</label>
				<span class="form-control"><?=date('d M Y', strtotime($data->tanggal));?></span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Agama :</label>
				<div class="detail-view"><?=nl2br($data->agama->agama);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Pendidikan :</label>
				<div class="detail-view"><?=nl2br($data->pendidikan->pendidikan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Jabatan :</label>
				<div class="detail-view"><?=nl2br($data->jabatan->jabatan);?></div>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Status :</label>
				<span class="form-control">
					<?php
						if ($data->active == 1){
							$text = "Active";
							$label = "success";
						} else 
						if ($data->active == 2){
							$text = "Deactive";
							$label = "warning";
						}
						echo "<span class='badge badge-" . $label . "'>". $text . "</span>";
					
					?>
				</span>
			</div>
			<div class="form-group col-xs-12">
				<label class="control-label">Last Modified by :</label>
				<span class="form-control"><?=$data->user_modify->username;?></span>
			</div>
		</div>
	</div>
<?php
	endif;
?>

