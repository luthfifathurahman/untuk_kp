<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <meta name="description" content="<?=getData('web_description');?>">
    <meta name="author" content="Fathurahman">
    <title><?=getData('web_title');?> | @yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link rel="apple-touch-icon" href="<?=url(getData('logo'));?>">
    <link rel="shortcut icon" href="<?=url(getData('logo'));?>">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?=url('vendors/assets/vendor/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/icofont/icofont.min.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/boxicons/css/boxicons.min.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/animate.css/animate.min.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/remixicon/remixicon.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/venobox/venobox.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/owl.carousel/assets/owl.carousel.min.css');?>" rel="stylesheet">
    <link href="<?=url('vendors/assets/vendor/aos/aos.css');?>" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?=url('vendors/assets/css/style.css');?>" rel="stylesheet">

</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">

            <h1 class="logo mr-auto"><a href="index.html"></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
            

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="<?=url('/home')?>">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#services">Services</a></li>
                    <li><a href="#team">Team</a></li>
                    <li><a href="#contact">Contact</a></li>

                </ul>
            </nav><!-- .nav-menu -->

            <a href="<?=url('backend/login');?>" class="get-started-btn">Login</a>

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

            <!-- Slide 1 -->
            <div class="carousel-item active" style="background-image: url(assets/img/slide/slide-1.jpg)">
            <div class="carousel-container">
                <div class="container">
                <h2 class="animate__animated animate__fadeInDown">Welcome to <span></span></h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                </div>
            </div>
            </div>

            <!-- Slide 2 -->
            <div class="carousel-item" style="background-image: url(assets/img/slide/slide-2.jpg)">
            <div class="carousel-container">
                <div class="container">
                <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                </div>
            </div>
            </div>

            <!-- Slide 3 -->
            <div class="carousel-item" style="background-image: url(assets/img/slide/slide-3.jpg)">
            <div class="carousel-container">
                <div class="container">
                <h2 class="animate__animated animate__fadeInDown">Sequi ea ut et est quaerat</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                </div>
            </div>
            </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

        </div>
    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
            <h2>About</h2>
            <p>About Us</p>
            </div>

            <div class="row content">
            <div class="col-lg-6">
                <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua.
                </p>
                <ul>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
                <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit</li>
                <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
                </ul>
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0">
                <p>
                Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <a href="#" class="btn-learn-more">Learn More</a>
            </div>
            </div>

        </div>
        </section><!-- End About Section -->

        <!-- ======= Counts Section ======= -->
        <section id="counts" class="counts">
        <div class="container" data-aos="fade-up">

            <div class="row no-gutters">

            <div class="col-lg-4 col-md-6 d-md-flex align-items-md-stretch">
                <div class="count-box ">
                <i class="icofont-simple-smile"></i>
                <span data-toggle="counter-up">232</span>
                <p><strong>Happy Clients</strong> consequuntur quae qui deca rode</p>
                <a href="#">Find out more &raquo;</a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6  d-md-flex align-items-md-stretch">
                <div class="count-box">
                <i class="icofont-document-folder"></i>
                <span data-toggle="counter-up">521</span>
                <p><strong>Projects</strong> adipisci atque cum quia aut numquam delectus</p>
                <a href="#">Find out more &raquo;</a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                <i class="icofont-live-support"></i>
                <span data-toggle="counter-up">1,463</span>
                <p><strong>Hours Of Support</strong> aut commodi quaerat. Aliquam ratione</p>
                <a href="#">Find out more &raquo;</a>
                </div>
            </div>


            </div>

        </div>
        </section><!-- End Counts Section -->

        <!-- ======= Services Section ======= -->
        <section id="services" class="services">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
            <h2>Layanan</h2>
            
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <a href="<?=url('backend/keluarga');?>">
                    <div class="icon-box">
                    <div class="icon"><i class="bx bxl-dribbble"></i></div>
                    <h4>Layanan Mandiri</h4>
                    <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
                    </div>
                    </a>
                </div>

            <div class="col-lg-6 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
                <div class="icon-box">
                <div class="icon"><i class="bx bx-tachometer"></i></div>
                <h4><a href="">Layanan Surat</a></h4>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                </div>
            </div>

            </div>

        </div>
        </section><!-- End Services Section -->

        <!-- ======= Team Section ======= -->
        <section id="team" class="team section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
            <h2>Team</h2>
            <p>Check our Team</p>
            </div>

            <div class="row">

            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="member" data-aos="zoom-in" data-aos-delay="100">
                <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">
                <div class="member-info">
                    <div class="member-info-content">
                    <h4>Walter White</h4>
                    <span>Chief Executive Officer</span>
                    </div>
                    <div class="social">
                    <a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>
                    <a href=""><i class="icofont-linkedin"></i></a>
                    </div>
                </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-md-6" data-wow-delay="0.1s">
                <div class="member" data-aos="zoom-in" data-aos-delay="200">
                <img src="assets/img/team/team-2.jpg" class="img-fluid" alt="">
                <div class="member-info">
                    <div class="member-info-content">
                    <h4>Sarah Jhonson</h4>
                    <span>Product Manager</span>
                    </div>
                    <div class="social">
                    <a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>
                    <a href=""><i class="icofont-linkedin"></i></a>
                    </div>
                </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-md-6" data-wow-delay="0.2s">
                <div class="member" data-aos="zoom-in" data-aos-delay="300">
                <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
                <div class="member-info">
                    <div class="member-info-content">
                    <h4>William Anderson</h4>
                    <span>CTO</span>
                    </div>
                    <div class="social">
                    <a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>
                    <a href=""><i class="icofont-linkedin"></i></a>
                    </div>
                </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-md-6" data-wow-delay="0.3s">
                <div class="member" data-aos="zoom-in" data-aos-delay="400">
                <img src="assets/img/team/team-4.jpg" class="img-fluid" alt="">
                <div class="member-info">
                    <div class="member-info-content">
                    <h4>Amanda Jepson</h4>
                    <span>Accountant</span>
                    </div>
                    <div class="social">
                    <a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>
                    <a href=""><i class="icofont-linkedin"></i></a>
                    </div>
                </div>
                </div>
            </div>

            </div>

        </div>
        </section><!-- End Team Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
            <h2>Contact</h2>
            <p>Contact Us</p>
            </div>

            <div class="row">
            <div class="col-lg-6">
                <div class="row">
                <div class="col-md-12">
                    <div class="info-box">
                    <i class="bx bx-map"></i>
                    <h3>Our Address</h3>
                    <p>A108 Adam Street, New York, NY 535022</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="info-box mt-3">
                    <i class="bx bx-envelope"></i>
                    <h3>Email</h3>
                    <p>info@example.com<br>contact@example.com</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="info-box mt-3">
                    <i class="bx bx-phone-call"></i>
                    <h3>Hubungi Kami</h3>
                    <p></p>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="info-box mt-3">
                    <i class="bx bx-map"></i>
                    <h3>Our Address</h3>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                    </div>
                    </div>
                </div>
                </div>

            </div>

            <div class="col-lg-6">
                <div class="info-box">
                <i class="bx bx-map"></i>
                <h3>Location</h3>
                <br>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15927.69019032493!2d98.47496410342191!3d3.6052056111261153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3030d61ef788f30d%3A0x1439833705db89e7!2sTangsi%2C%20Binjai%20Kota%2C%20Binjai%20City%2C%20North%20Sumatra!5e0!3m2!1sen!2sid!4v1610037667644!5m2!1sen!2sid" width="500" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>

            <!--<div class="col-lg-6 info-box" id="kopi-covid">
            </div>
            <script>
                var f = document.createElement("iframe");
                f.src = "https://kopi.dev/widget-covid-19/";
                f.width = "100%";
                f.height = 340;
                f.scrolling = "no";
                f.frameBorder = 0;
                var rootEl = document.getElementById("kopi-covid");
                console.log(rootEl);
                rootEl.appendChild(f);
            </script>
            -->
            </div>

        </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

        <!-- footer content -->
        @include('frontend.partials.footer')
        <!-- /footer content -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?=url('vendors/assets/vendor/jquery/jquery.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/jquery.easing/jquery.easing.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/php-email-form/validate.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/waypoints/jquery.waypoints.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/counterup/counterup.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/venobox/venobox.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/owl.carousel/owl.carousel.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/isotope-layout/isotope.pkgd.min.js');?>"></script>
  <script src="<?=url('vendors/assets/vendor/aos/aos.js');?>"></script>

  <!-- Template Main JS File -->
  <script src="<?=url('vendors/assets/js/main.js');?>"></script>

</body>

</html>