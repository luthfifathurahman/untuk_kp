-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2021 at 09:35 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_database_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_control`
--

CREATE TABLE `access_control` (
  `id` int(11) NOT NULL,
  `user_level_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_control`
--

INSERT INTO `access_control` (`id`, `user_level_id`, `module_id`, `content`, `user_modified`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'a', 1, '2017-10-17 09:26:05', '2017-10-17 09:26:05'),
(2, 1, 2, 'a', 1, '2017-10-17 09:26:05', '2017-10-17 09:26:05'),
(3, 1, 3, 'a', 1, '2017-10-17 09:26:05', '2017-10-17 09:26:05'),
(4, 2, 1, 'v', 1, '2017-10-17 09:26:13', '2020-11-12 15:49:10'),
(5, 2, 2, 'vu', 1, '2017-10-17 09:26:13', '2020-12-18 03:23:14'),
(6, 2, 3, 'v', 1, '2017-10-17 09:26:13', '2020-11-12 15:49:10'),
(7, 3, 1, 'v', 1, '2017-10-17 09:26:18', '2018-06-03 06:05:32'),
(8, 3, 2, 'v', 1, '2017-10-17 09:26:18', '2018-06-03 06:05:32'),
(9, 3, 3, 'v', 1, '2017-10-17 09:26:18', '2018-06-03 06:05:32'),
(10, 1, 4, 'a', 1, '2018-07-02 06:15:50', '2018-07-02 06:15:50'),
(11, 2, 4, 'vu', 1, '2018-07-02 06:15:53', '2020-12-18 03:23:14'),
(12, 3, 4, 'v', 1, '2018-07-02 06:15:56', '2018-07-02 06:15:56'),
(13, 1, 5, 'a', 1, '2018-07-02 08:28:39', '2018-07-02 08:28:39'),
(14, 2, 5, 'vu', 1, '2018-07-02 08:28:43', '2020-12-18 03:23:14'),
(15, 3, 5, 'v', 1, '2018-07-02 08:28:45', '2018-07-02 08:28:45'),
(16, 1, 6, 'a', 1, '2018-07-03 07:02:26', '2018-07-03 07:02:26'),
(17, 2, 6, 'v', 1, '2018-07-03 07:02:29', '2020-11-12 15:49:10'),
(18, 3, 6, 'v', 1, '2018-07-03 07:02:31', '2018-07-03 07:02:31'),
(19, 1, 7, 'a', 1, '2018-07-03 08:32:29', '2018-07-03 08:32:29'),
(20, 2, 7, 'v', 1, '2018-07-03 08:32:32', '2020-11-12 15:49:10'),
(21, 3, 7, 'v', 1, '2018-07-03 08:32:35', '2018-07-03 08:32:35'),
(22, 1, 8, 'a', 1, '2018-07-03 12:39:25', '2018-07-03 12:39:25'),
(23, 2, 8, 'v', 1, '2018-07-03 12:39:30', '2020-11-28 16:50:13'),
(24, 3, 8, 'v', 1, '2018-07-03 12:39:34', '2018-07-03 12:39:34'),
(25, 1, 9, 'a', 1, '2021-01-07 18:37:59', '2021-01-07 18:37:59'),
(26, 1, 10, 'a', 1, '2021-01-14 17:00:52', '2021-01-14 17:00:52'),
(27, 1, 11, 'a', 1, '2021-01-15 14:52:44', '2021-01-15 14:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `user_modified` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id`, `judul`, `isi`, `kategori_id`, `user_modified`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Ujicoba 1', '<p>Ini&nbsp;<strong>adalah&nbsp;<em>suatu&nbsp;<s>Percobaan</s></em></strong></p>\r\n\r\n<ol>\r\n	<li><strong><em><s>adalah 2</s></em><s>2w2w2</s></strong></li>\r\n</ol>', 1, 1, '2021-01-16 17:58:42', '2021-01-16 17:58:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `no_kk` varchar(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `lingkungan` varchar(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `active` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `no_kk`, `nama`, `alamat`, `lingkungan`, `created_at`, `updated_at`, `user_modified`, `active`) VALUES
(3, '1275021510090001', 'Edi Suratman', 'Jl. Dulu Gan', '1', '2020-12-03 19:57:01', '2020-12-03 19:57:01', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact` varchar(45) DEFAULT NULL,
  `subject` varchar(150) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `read` int(1) DEFAULT NULL,
  `active` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `media_library`
--

CREATE TABLE `media_library` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `size` varchar(10) DEFAULT NULL,
  `user_created` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_library`
--

INSERT INTO `media_library` (`id`, `name`, `type`, `url`, `size`, `user_created`, `created_at`, `updated_at`) VALUES
(0, 'noprofileimage', 'png', 'img/noprofileimage.png', '1159', 1, '2017-05-29 19:56:03', '2017-05-29 19:56:03'),
(1, 'tumblr_o8dmovrRZ21v9dlz9o9_250', 'png', 'upload/img/tumblr_o8dmovrRZ21v9dlz9o9_250.png', '76098', 1, '2018-07-03 06:40:43', '2018-07-03 06:40:43'),
(2, 'tumblr_oo8kv5a44d1s81jsfo2_250', 'png', 'upload/img/tumblr_oo8kv5a44d1s81jsfo2_250.png', '76301', 1, '2018-07-03 06:40:43', '2018-07-03 06:40:43'),
(3, 'EQZI6m4VAAAuLCX', 'jpg', 'upload/img/EQZI6m4VAAAuLCX.jpg', '248843', 1, '2021-01-07 18:58:40', '2021-01-07 18:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_11_29_211450_create_penduduk_table', 1),
(8, '2021_01_04_000905_create_ref_pemerintahan_table', 2),
(9, '2021_01_04_001040_create_ref_pendidikan_table', 2),
(10, '2021_01_04_001121_create_ref_pekerjaan_table', 2),
(11, '2021_01_04_001154_create_ref_jenkel_table', 2),
(12, '2021_01_04_001329_create_ref_jabatan_table', 2),
(13, '2021_01_04_001415_create_ref_kewarganegaraan_table', 2),
(16, '2021_01_15_234621_create_artikel_table', 3),
(17, '2021_01_15_235516_create_ref_kategori_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(20) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `slug`, `active`, `user_modified`, `created_at`, `updated_at`) VALUES
(1, 'Master User Level', 'users-level', 0, 1, '2017-10-17 07:07:07', '2020-12-10 18:11:22'),
(2, 'Pengguna', 'users-user', 1, 1, '2017-10-17 07:16:51', '2020-12-10 18:11:10'),
(3, 'Media Library', 'media-library', 0, 1, '2017-10-17 07:19:28', '2020-12-10 18:11:18'),
(4, 'Data Penduduk', 'penduduk', 1, 1, '2018-07-02 06:15:45', '2020-12-02 17:34:44'),
(5, 'Data Keluarga', 'keluarga', 1, 1, '2018-07-02 08:28:31', '2021-01-07 18:37:35'),
(6, 'Purchase Order', 'purchase-order', 0, 1, '2018-07-03 07:02:20', '2020-12-10 18:10:41'),
(7, 'Daftar Inden', 'inden', 0, 1, '2018-07-03 08:32:24', '2020-12-10 18:10:47'),
(8, 'Penjualan', 'penjualan', 0, 1, '2018-07-03 12:39:19', '2020-12-10 18:10:51'),
(9, 'Data Pemerintahan', 'pemerintahan', 1, 1, '2021-01-07 18:37:27', '2021-01-07 18:37:27'),
(10, 'Informasi Kelurahan', 'informasi', 1, 1, '2021-01-14 17:00:42', '2021-01-14 17:00:42'),
(11, 'Artikel', 'artikel', 1, 1, '2021-01-15 14:52:10', '2021-01-15 14:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `pemerintahan`
--

CREATE TABLE `pemerintahan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nik` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_id` int(10) UNSIGNED NOT NULL,
  `tempat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jenkel_id` int(10) UNSIGNED NOT NULL,
  `pendidikan_id` int(10) UNSIGNED NOT NULL,
  `agama_id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `active` int(1) NOT NULL,
  `user_modified` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemerintahan`
--

INSERT INTO `pemerintahan` (`id`, `nik`, `nip`, `nama`, `avatar_id`, `tempat`, `tanggal`, `jenkel_id`, `pendidikan_id`, `agama_id`, `jabatan_id`, `active`, `user_modified`, `created_at`, `updated_at`) VALUES
(1, '1275021910080001', '1212324324123231', 'Dhea Angelina', 3, 'Jakarta', NULL, 2, 8, 2, 2, 1, 1, '2021-01-07 19:18:37', '2021-01-07 19:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_d`
--

CREATE TABLE `penjualan_d` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_penjualan` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_d`
--

INSERT INTO `penjualan_d` (`id`, `id_barang`, `jumlah`, `harga`, `created_at`, `updated_at`, `id_penjualan`) VALUES
(1, 2, 2, 55000, '2018-07-04 03:22:20', '2018-07-04 03:22:20', 1),
(2, 1, 3, 27000, '2018-07-04 03:22:20', '2018-07-04 03:22:20', 1),
(3, 1, 1, 27000, '2018-07-04 03:27:31', '2018-07-04 03:27:31', 2),
(4, 2, 2, 55000, '2018-07-04 03:27:31', '2018-07-04 03:27:31', 2);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_h`
--

CREATE TABLE `penjualan_h` (
  `id` int(11) NOT NULL,
  `no_nota` varchar(45) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_h`
--

INSERT INTO `penjualan_h` (`id`, `no_nota`, `tanggal`, `total`, `active`, `created_at`, `updated_at`, `user_modified`, `keterangan`) VALUES
(1, 'NOT/0001', '2018-07-04', 191000, 0, '2018-07-04 03:22:20', '2018-07-04 03:26:44', 1, 'Keterangan Penjualan'),
(2, 'NOT/0001', '2018-07-04', 137000, 1, '2018-07-04 03:27:31', '2018-07-04 03:27:31', 1, 'Keterangan Penjualan');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_d`
--

CREATE TABLE `purchase_d` (
  `id` int(11) NOT NULL,
  `id_purchase` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_d`
--

INSERT INTO `purchase_d` (`id`, `id_purchase`, `id_barang`, `jumlah`, `harga`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 5, 25000, '2018-07-03 07:38:25', '2018-07-03 07:38:25'),
(2, 1, 2, 10, 50000, '2018-07-03 07:38:25', '2018-07-03 07:38:25'),
(4, 2, 2, 5, 65000, '2018-07-03 08:08:04', '2018-07-03 08:08:04'),
(5, 2, 1, 15, 30000, '2018-07-03 08:08:04', '2018-07-03 08:08:04');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_h`
--

CREATE TABLE `purchase_h` (
  `id` int(11) NOT NULL,
  `no_inv` varchar(45) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `id_sup` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `status` enum('order','received') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_h`
--

INSERT INTO `purchase_h` (`id`, `no_inv`, `total`, `id_sup`, `active`, `status`, `created_at`, `updated_at`, `user_modified`, `tanggal`, `keterangan`) VALUES
(1, 'INV/0001', 625000, 2, 1, 'received', '2018-07-03 07:38:25', '2018-07-03 07:39:13', 1, '2018-07-03', NULL),
(2, 'INV/0002', 775000, 3, 1, 'received', '2018-07-03 07:41:03', '2020-11-28 16:52:18', 1, '2018-07-03', 'Keterangan Purchase');

-- --------------------------------------------------------

--
-- Table structure for table `ref_agama`
--

CREATE TABLE `ref_agama` (
  `id` int(11) NOT NULL,
  `agama` varchar(20) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_agama`
--

INSERT INTO `ref_agama` (`id`, `agama`) VALUES
(1, 'Islam'),
(2, 'Kristen Protestan'),
(3, 'Katholik'),
(4, 'Hindu'),
(5, 'Buddha'),
(6, 'Konghucu');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jabatan`
--

CREATE TABLE `ref_jabatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jabatan`
--

INSERT INTO `ref_jabatan` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'Lurah', '2021-01-07 19:09:02', '2021-01-07 19:09:06'),
(2, 'Sekretaris Lurah', '2021-01-07 19:09:02', '2021-01-07 19:09:06'),
(3, 'Kasi Pemerintahan', '2021-01-07 19:09:02', '2021-01-07 19:09:06'),
(4, 'Kasi PMK dan Kesos', '2021-01-07 19:09:02', '2021-01-07 19:09:06'),
(5, 'Kasi Trantib dan Pelayanan Umum', '2021-01-07 19:09:02', '2021-01-07 19:09:06');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenkel`
--

CREATE TABLE `ref_jenkel` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenkel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenkel`
--

INSERT INTO `ref_jenkel` (`id`, `jenkel`, `created_at`, `updated_at`) VALUES
(1, 'Laki-Laki', '2021-01-05 15:11:07', '2021-01-05 15:11:10'),
(2, 'Perempuan', '2021-01-05 15:11:37', '2021-01-05 15:11:40');

-- --------------------------------------------------------

--
-- Table structure for table `ref_kategori`
--

CREATE TABLE `ref_kategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ref_kategori`
--

INSERT INTO `ref_kategori` (`id`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'berita', '2021-01-04 17:53:48', '2021-01-04 17:53:48');

-- --------------------------------------------------------

--
-- Table structure for table `ref_kewarganegaraan`
--

CREATE TABLE `ref_kewarganegaraan` (
  `id` int(10) UNSIGNED NOT NULL,
  `kewarganegaraan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_kewarganegaraan`
--

INSERT INTO `ref_kewarganegaraan` (`id`, `kewarganegaraan`, `created_at`, `updated_at`) VALUES
(1, 'WNI', '2021-01-04 17:53:48', '2021-01-04 17:53:48'),
(2, 'WNA', '2021-01-04 17:54:48', '2021-01-04 17:55:48');

-- --------------------------------------------------------

--
-- Table structure for table `ref_pekerjaan`
--

CREATE TABLE `ref_pekerjaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `pekerjaan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_pekerjaan`
--

INSERT INTO `ref_pekerjaan` (`id`, `pekerjaan`, `created_at`, `updated_at`) VALUES
(1, 'Belum / Tidak Bekerja', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(2, 'Mengurus Rumah Tangga', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(3, 'Pelajar / Mahasiswa', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(4, 'Pensiunan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(5, 'Pegawai Negeri Sipil (PNS)', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(6, 'Tentara Nasional Indonesia (TNI)', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(7, 'Kepolisian RI (POLRI)', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(8, 'Perdagangan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(9, 'Petani / Pekebun', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(10, 'Peternak', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(11, 'Nelayan / Perikanan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(12, 'Industri', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(13, 'Konstruksi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(14, 'Transportasi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(15, 'Karyawan Swasta', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(16, 'Karyawan BUMN', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(17, 'Karyawan BUMD', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(18, 'Karyawan Honorer', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(19, 'Buruh Harian Lepas', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(20, 'Buruh Tani / Perkebunan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(21, 'Buruh Nelayan /  Perikanan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(22, 'Buruh Peternakan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(23, 'Pembantu Rumah Tangga', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(24, 'Tukang Cukur', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(25, 'Tukang Listrik', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(26, 'Tukang Batu', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(27, 'Tukang Kayu', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(28, 'Tukang Sol Sepatu', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(29, 'Tukang Las / Pandai Besi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(30, 'Tukang Jahit', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(31, 'Tukang Gigi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(32, 'Penata Rias', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(33, 'Penata Busana', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(34, 'Penata Rambut', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(35, 'Mekanik', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(36, 'Seniman', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(37, 'Tabib', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(38, 'Paraji', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(39, 'Perancang Busana', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(40, 'Penerjemah', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(41, 'Imam Masjid', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(42, 'Pendeta', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(43, 'Pastor', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(44, 'Wartawan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(45, 'Ustadz / Mubaligh', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(46, 'Juru Masak', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(47, 'Promotor Acara', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(48, 'Anggota DPR-RI', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(49, 'Anggota DPD', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(50, 'Anggota BPK', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(51, 'Presiden', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(52, 'Wakil Presiden', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(53, 'Anggota Mahkamah Konstitusi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(54, 'Anggota Kabinet Kementerian', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(55, 'Duta Besar', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(56, 'Gubernur', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(57, 'Wakil Gubernur', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(58, 'Bupati', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(59, 'Wakil Bupati', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(60, 'Walikota', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(61, 'Wakil Walikota', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(62, 'Anggota DPRD Provinsi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(63, 'Anggota DPRD Kabupaten / Kota', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(64, 'Dosen', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(65, 'Guru', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(66, 'Pilot', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(67, 'Pengacara', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(68, 'Notaris', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(69, 'Arsitek', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(70, 'Akuntan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(71, 'Konsultan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(72, 'Dokter', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(73, 'Bidan', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(74, 'Perawat', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(75, 'Apoteker', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(76, 'Psikiater / Psikolog', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(77, 'Penyiar Televisi', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(78, 'Penyiar Radio', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(79, 'Pelaut', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(80, 'Peneliti', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(81, 'Sopir', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(82, 'Pialang', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(83, 'Paranormal', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(84, 'Pedagang', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(85, 'Biarawati', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(86, 'Wiraswasta', '2021-01-05 16:23:12', '2021-01-05 16:23:14'),
(87, 'Lainnya', '2021-01-06 16:58:59', '2021-01-06 16:58:59');

-- --------------------------------------------------------

--
-- Table structure for table `ref_pendidikan`
--

CREATE TABLE `ref_pendidikan` (
  `id` int(10) UNSIGNED NOT NULL,
  `pendidikan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_pendidikan`
--

INSERT INTO `ref_pendidikan` (`id`, `pendidikan`, `created_at`, `updated_at`) VALUES
(1, 'Tidak / Belum Sekolah', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(2, 'Belum Tamat SD / Sederajat', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(3, 'Tamat SD / Sederajat', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(4, 'SLTP / Sederajat', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(5, 'SLTP / Sederajat', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(6, 'Diploma I / II', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(7, 'Akademi / Diploma III / S. Muda', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(8, 'Diploma IV / Strata I (S1)', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(9, 'Strata II (S2)', '2021-01-05 15:36:11', '2021-01-05 15:36:14'),
(10, 'Strata III (S3)', '2021-01-05 15:36:11', '2021-01-05 15:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `value` text DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `user_modified`, `created_at`, `updated_at`) VALUES
(1, 'web_title', 'Kelurahan Tangsi', 1, '2017-06-13 00:27:16', '2020-12-01 16:34:59'),
(2, 'logo', 'img/logo.png', 1, '2017-06-13 00:27:16', '2018-06-03 05:58:24'),
(3, 'email_admin', 'admin@admin.com', 1, '2017-06-13 00:27:16', '2018-06-03 05:58:52'),
(4, 'web_description', 'Sistem Informasi Administrasi Kependudukan (SIAK)', 1, '2017-07-23 23:56:28', '2020-12-22 15:38:54');

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `type` enum('beli','jual','koreksi') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`id`, `id_barang`, `keterangan`, `jumlah`, `type`, `created_at`, `updated_at`) VALUES
(22, 2, 'Koreksi Stok', 6, 'koreksi', '2018-07-04 10:12:54', '2018-07-04 10:12:54'),
(21, 2, 'Koreksi Stok', -6, 'koreksi', '2018-07-04 10:12:26', '2018-07-04 10:12:26'),
(20, 2, 'NOT/0001', -2, 'jual', '2018-07-04 03:27:31', '2018-07-04 03:27:31'),
(19, 1, 'NOT/0001', -1, 'jual', '2018-07-04 03:27:31', '2018-07-04 03:27:31'),
(15, 1, 'INV/0001', 5, 'beli', '2018-07-03 07:39:13', '2018-07-03 07:39:13'),
(16, 2, 'INV/0001', 10, 'beli', '2018-07-03 07:39:13', '2018-07-03 07:39:13'),
(23, 2, 'INV/0002', 5, 'beli', '2020-11-28 16:52:18', '2020-11-28 16:52:18'),
(24, 1, 'INV/0002', 15, 'beli', '2020-11-28 16:52:18', '2020-11-28 16:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat` varchar(30) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenkel_id` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `rt` varchar(5) DEFAULT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kewarganegaraan_id` int(11) NOT NULL,
  `agama_id` int(11) NOT NULL,
  `pendidikan_id` int(11) NOT NULL,
  `pekerjaan_id` varchar(11) NOT NULL,
  `active` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `nik`, `nama`, `tempat`, `tgl_lahir`, `jenkel_id`, `alamat`, `rt`, `kelurahan`, `kecamatan`, `kewarganegaraan_id`, `agama_id`, `pendidikan_id`, `pekerjaan_id`, `active`, `created_at`, `updated_at`, `user_modified`) VALUES
(4, '1275021010010001', 'Ronggoweni', 'Laksanapura', '2020-10-20', 2, 'Jl. Merdeka Barat', '1', 'Tangsi', 'Binjai Kota', 1, 1, 2, '2', 1, '2020-11-29 17:07:22', '2021-01-05 17:10:34', 1),
(5, '1275021910080001', 'Mahabrata', 'India', '2019-10-20', 0, 'Jl. Dulu Gan', '1', 'Tangsi', 'Binjai Kota', 0, 0, 0, 'Tukang Batu', 0, '2020-12-18 03:34:58', '2021-01-01 18:38:34', 1),
(6, '1275021010010001', 'Adam Suanto', 'Binjai', '2020-01-20', 1, 'Jl. Dulu Gan', '1', 'Tangsi', 'Binjai Kota', 1, 3, 2, '16', 1, '2021-01-01 18:39:13', '2021-01-05 17:11:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_level_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `gender` enum('male','female','other') DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` int(1) NOT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_level_id`, `firstname`, `lastname`, `avatar_id`, `email`, `address`, `phone`, `gender`, `birthdate`, `username`, `password`, `active`, `user_modified`, `last_activity`, `created_at`, `updated_at`) VALUES
(1, 1, 'Super', 'Admin', 3, 'superadmin@admin.com', 'Jl Madura xxxxxxx', '08383xxxxxxx', 'male', '1986-07-25', 'superadmin', '$2y$10$TkX/dDYrtvIEXidPOag5T.V8qbyluUHJg5ssBjKe6WlVqpItuN8uy', 1, 1, '2021-01-16 17:19:18', '2017-03-13 20:51:35', '2021-01-16 17:19:18'),
(2, 2, 'Admin', 'Admin', 0, 'admin@admin.com', NULL, NULL, 'male', NULL, 'admin', '$2y$10$PQaUY4b0YsSo5qAuK8Cc.OB.WeEJHrJJ0FDgk6YE9xhXboVRou3We', 1, 1, '2020-12-01 17:24:52', '2017-04-19 14:29:01', '2020-12-01 17:24:52');

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE `user_levels` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `active` int(1) DEFAULT NULL,
  `user_modified` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`id`, `name`, `active`, `user_modified`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 1, 1, '2017-06-28 06:18:17', '2017-06-28 06:18:17'),
(2, 'Admin', 1, 1, '2018-06-02 15:59:51', '2018-06-02 15:59:51'),
(3, 'User', 1, 1, '2018-06-03 04:19:49', '2018-06-03 04:19:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_control`
--
ALTER TABLE `access_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_library`
--
ALTER TABLE `media_library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemerintahan`
--
ALTER TABLE `pemerintahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_d`
--
ALTER TABLE `penjualan_d`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan_h`
--
ALTER TABLE `penjualan_h`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_d`
--
ALTER TABLE `purchase_d`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_h`
--
ALTER TABLE `purchase_h`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_agama`
--
ALTER TABLE `ref_agama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_jabatan`
--
ALTER TABLE `ref_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_jenkel`
--
ALTER TABLE `ref_jenkel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_kategori`
--
ALTER TABLE `ref_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_kewarganegaraan`
--
ALTER TABLE `ref_kewarganegaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_pekerjaan`
--
ALTER TABLE `ref_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ref_pendidikan`
--
ALTER TABLE `ref_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_levels`
--
ALTER TABLE `user_levels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_control`
--
ALTER TABLE `access_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_library`
--
ALTER TABLE `media_library`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pemerintahan`
--
ALTER TABLE `pemerintahan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `penjualan_d`
--
ALTER TABLE `penjualan_d`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `penjualan_h`
--
ALTER TABLE `penjualan_h`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchase_d`
--
ALTER TABLE `purchase_d`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `purchase_h`
--
ALTER TABLE `purchase_h`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_agama`
--
ALTER TABLE `ref_agama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ref_jabatan`
--
ALTER TABLE `ref_jabatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ref_jenkel`
--
ALTER TABLE `ref_jenkel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_kategori`
--
ALTER TABLE `ref_kategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_kewarganegaraan`
--
ALTER TABLE `ref_kewarganegaraan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_pekerjaan`
--
ALTER TABLE `ref_pekerjaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `ref_pendidikan`
--
ALTER TABLE `ref_pendidikan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_levels`
--
ALTER TABLE `user_levels`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
